import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class EmployeeService {
    private baseapi = environment.apiUrl;
    constructor(private http: HttpClient) { }

    getallemployees() {
        return this.http.get(this.baseapi + "/employee/getall");
    }

    getalltasks() {
        return this.http.get(this.baseapi + "/task/getall");
    }

    getEmployeeTimeSheet(id) {
        return this.http.get(this.baseapi + "/timesheet/getemployeetimesheetbyid?EmployeeId=" + id)
    }

    addNewTimeSheet(data) {
        console.log(data);
        return this.http.post(this.baseapi + "/timesheet/addemployeetimesheet", data);
    }
}